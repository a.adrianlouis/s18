// console.log("Hi");


// ===== Addition

function addNumber(num1, num2){
	let add = num1 + num2; 
	
	console.log("Displayed sum of " + num1 + " and " + num2);

	console.log(add);

}

addNumber(5, 15);



// ===== Subtraction

function subNumber(num1, num2){
	let sub = num1 - num2; 
	
	console.log("Displayed difference of " + num1 + " and " + num2);

	console.log(sub);

}

subNumber(20, 5);



// ===== Multiplication

function multiplyNumber(num1, num2){

	console.log("Displayed product of " + num1 + " and " + num2 + ":");

	return multiply = num1 * num2; 

}

multiplyNumber(50, 10);
console.log(multiply);



// ===== Division

function divideNumber(num1, num2){

	console.log("Displayed quotient of " + num1 + " and " + num2 + ":");

	return multiply = num1 / num2; 

}

divideNumber(50, 10);
console.log(multiply);



// ===== Area of a Circle

function areaOfACircle(area) {
	const pi = 3.14159;
	let radius = area;

	console.log("The result of getting the area of a circle with " + area + " radius:")

	return circleArea = pi * area * area;

}

areaOfACircle(15);
console.log(circleArea);





// ===== Average

function average(num1, num2, num3, num4){
	
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and" + num4 + ":");

	let total = num1 + num2 + num3 + num4;
	return averageVar = total / 4

}

average(20, 40, 60, 80);
console.log(averageVar);





// ===== Passing Score

function passingScore(num1, num2){
	let score = num1 / num2;
	let multiplier = score * 100;

	console.log("Is " + num1 + "/" + num2 + " a passing score?")
	return ifPassed = multiplier > 75;

}

passingScore(38, 50);
console.log(ifPassed);
